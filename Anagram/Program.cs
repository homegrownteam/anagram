﻿using System;
using System.Text.RegularExpressions;

namespace SplitStrings
{
    class Program
    {
        static void Main(string[] args)
        {
                     
           string source; //Source string for reversing
           
           bool CheckExit; //Checking exit conditions
            
            do
            {
                Console.Write("Please enter a string (type 'exit' to finish): ");
                
                source = Console.ReadLine();

                CheckExit = string.Equals(source, "exit", StringComparison.OrdinalIgnoreCase);
                                               
                if (!CheckExit)
                {
                    Console.Write("The reversed string: ");
                    
                    ReverseSting(source);
                }
                    
                Console.WriteLine("");

            } while (!CheckExit) ;

            Console.WriteLine("See you next time!");


        }

        static void ReverseSting(string source)
        {
            string[] words = source.Split(" "); //split a string into the words
            
            foreach(var word in words) //a string processing loop
            {
                char[] chars = word.ToCharArray(); //convert a word into the array of chars

                var m = chars.Length; 

                char tchar; //a temporary variable for chars' exchange buffer

                var cntr = 1; //a counter for shifting from the right end of word

                for (var j = 0; j < m-cntr; j++) //the chars processing loop
                {

                    if (Char.IsLetter(chars[j]) == false) //eliminating non-letter chars from the left side of word
                    {
                        continue;
                    }
                    
                    while (Char.IsLetter(chars[m - cntr]) == false) //eliminating non-letter chars from the right side of word
                    {
                        cntr++;
                    }

                    //chars' swapping
                    tchar = chars[j];
                    chars[j] = chars[m - cntr];
                    chars[m - cntr] = tchar;
                    cntr++;
                }

                Console.Write($"{new string(chars)} "); //print out the reversed string          
                
            }

        }
    }
}
